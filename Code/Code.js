/*********************************************************************
8 October 2021

This code has been written as a part of the MSc thesis "Sustainability 
of groundwater resources in Kumasi, Ghana" by Estela Fernandes Potter.

PART 2: GROUNDWATER RECHARGE IN KUMASI
This part uses a supervised classification and Random Forest classifier
to classify 1986, 2013, and 2020 surface reflectance images from Landsat 5,
7, 8 and Sentinel-2.

Required import of 
1. 2020 classification map over Kumasi: "classification2020_S2.tif"
2. 2013 classification map over Kumasi: "classification2013_L8.tif"
3. 2003 classification map over Kumasi: "classification2003.tif" (optional)
4. 1986 classification map over Kumasi: "classification1986_L5.tif"
5. Hydrological soil groups: "HSG.kumasi.tif"
6. Kumasi outline: "Kumasi_outline.zip" 

1. Download classification images, Kumasi shapefile, and hydrological soil group (HSG) map
   from INSERT URL
2. Under the 'Assets' dock (left), upload the classification images (raster), HSG (raster)
   outline of Kumasi (shapefile).
3. Import these into the script as shown below. Username and file location must change
   depending on the local username.


Note #1: for rasters use the sample pyramiding policy.
Note #2: see line 477 if 1986 or 2013 classifications are chosen
***********************************************************************/

var assetList = ee.data.listAssets("user/coworkername/somefolder");
print(assetList)

// Import shapefile of study area
//var Kumasi = ee.FeatureCollection('users/civil05/Kumasi_outline');
var Kumasi = ee.FeatureCollection('users/civil05/Syi_District_Boundary');

// Import classification images
//var classification = ee.Image("users/civil05/classification1986_L5");
//var classification = ee.Image("users/civil05/classification2020_S2");
//var classification = ee.Image("users/civil05/classification2013_L8"); 
//var classification = ee.Image("users/civil05/classification2003"); // optional

var classification = ee.Image("users/civil05/Classification_2022u"); 

// Import HSG raster. HSG's have been converted from  "A,B,C,D" to "1,2,3,4"
//var HSG = ee.Image('users/estelafpotter/HSG_raster2_samplepyr');
//var HSG = ee.Image('users/civil05/HSG_kumasi');

var HSG = ee.Image('users/civil05/HSG_SY_H2');

// Retreive geometry of Kumasi outline
var geometry = Kumasi.geometry();
Map.centerObject(geometry);


//////////////////////////////////////////
//           IMAGE COLLECTION           //
//////////////////////////////////////////

// CLOUD MASK FUNCTIONS

/** Landsat: 
 * Function to mask clouds based on the pixel_qa band of Landsat SR data.
 * @param {ee.Image} image Input Landsat SR image
 * @return {ee.Image} Cloudmasked Landsat image
 */
 
var cloudMaskL457 = function(image) {
  var qa = image.select('pixel_qa');
  // If the cloud bit (5) is set and the cloud confidence (7) is high
  // or the cloud shadow bit is set (3), then it's a bad pixel.
  var cloud = qa.bitwiseAnd(1 << 5)
                  .and(qa.bitwiseAnd(1 << 7))
                  .or(qa.bitwiseAnd(1 << 3));
  // Remove edge pixels that don't occur in all bands
  var mask2 = image.mask().reduce(ee.Reducer.min());
  return image.updateMask(cloud.not()).updateMask(mask2);
};

/* Sentinel 2: cloud mask function (https://developers.google.com/earth-engine/datasets/catalog/COPERNICUS_S2_SR#description)
 * Function to mask clouds using the Sentinel-2 QA band
 * @param {ee.Image} image Sentinel-2 image
 * @return {ee.Image} cloud masked Sentinel-2 image
 */
 
function maskS2clouds(image) {
  var qa = image.select('QA60');

  // Bits 10 and 11 are clouds and cirrus, respectively.
  var cloudBitMask = 1 << 10;
  var cirrusBitMask = 1 << 11;

  // Both flags should be set to zero, indicating clear conditions.
  var mask = qa.bitwiseAnd(cloudBitMask).eq(0)
      .and(qa.bitwiseAnd(cirrusBitMask).eq(0));

  return image.updateMask(mask).divide(10000);
}


///////////////////////////////////////////
//           CLASSIFICATION             //
///////////////////////////////////////////


// Select classification map of interest and rename band with classification as 'classification'
var classified = classification.rename('classification');

// Visualisation parameters of land use map
var palette =
  ['737373',  //(0) Urban semi-pervious
  '800000',   //(1) Urban impervious
  'e6ac00',   //(2) Bare land
  '006600',   //(3) Dense vegetation
  '86b300',   //(4) Sparse vegetation
  '003399'];  //(5) Water


// Legend for LULC map

// set position of panel
var legend = ui.Panel({
  style: {
    position: 'bottom-left',
    padding: '8px 15px'
  }
});
 
// Create legend title
var legendTitle = ui.Label({
  value: 'Land classes',
  style: {
    fontWeight: 'bold',
    fontSize: '18px',
    margin: '0 0 4px 0',
    padding: '0'
    }
});

// Add the title to the panel
legend.add(legendTitle);
 
// Creates and styles 1 row of the legend.
var makeRow = function(color, name) {
 
      // Create the label that is actually the colored box.
      var colorBox = ui.Label({
        style: {
          backgroundColor: '#' + color,
          // Use padding to give the box height and width.
          padding: '8px',
          margin: '0 0 4px 0'
        }
      });
 
      // Create the label filled with the description text.
      var description = ui.Label({
        value: name,
        style: {margin: '0 0 4px 6px'}
      });
 
      // return the panel
      return ui.Panel({
        widgets: [colorBox, description],
        layout: ui.Panel.Layout.Flow('horizontal')
      });
};
 
// Name of the legend
var names = ['Urban (semipervious)','Urban (impervious)','Bare land','Dense vegetation','Sparse vegetation','Water'];
 
// Add color and and names
for (var i = 0; i < 6; i++) {
  legend.add(makeRow(palette[i], names[i]));
  }  

Map.add(legend);

////////////////////////////////////////////
//          HYDROLOGICAL SOIL GROUPS      //
////////////////////////////////////////////

// Visualisation of group A, B, D (1, 2, 4)
var paletteHSG = ['c0c0c0','990099','33CC00']

// Add HSG to existing legend
 
// HSG group names
var groups = ['HSG A','HSG B','HSG D'];
 
// Add color and and names. 'i' describes the amount of variables
for (var i = 0; i < 3; i++) {
  legend.add(makeRow(paletteHSG[i], groups[i]));
  }  
  
Map.addLayer(HSG, {min: 1, max: 4, palette: paletteHSG}, 'Hydrological soil groups')

////////////////////////////////////////////
//              CURVE NUMBERS             //
////////////////////////////////////////////

print('RESULTS PANEL')
print('1. CURVE NUMBER ESTIMATES')

/* Use USDA tables to retreive curve numbers for specific group combinations. 

Below are CNs for normal AMC conditions.

HSG raster notation      1     2     4
(0)Urban semipervious	74	  83.5	90
(1)Urban impervious	    98	  98	98
(2)Bare	                68	  79	89
(3)Dense vegetation	    30    55	77
(4)Sparse vegetation	36.75 60    79.25
(5)Water                100   100   100

*/

// Clipped land use classification
var LULC = classified.clip(geometry);
print('Classification info: ',LULC);

Map.addLayer(classified, {min: 0, max: 5, palette: palette}, 'Land cover classification')

// Create lists for Curve Number selection based on land use class
var land_classes = ee.List([0, 1, 2, 3, 4, 5]);

/*
Average, minimum and maximum curve numbers per land use class are given below. 
Select the average curve numbers for runoff estimates. To investigate CN ranges
select minimum or maximum curve numbers.
*/

// AVERAGE CURVE NUMBERS
var CNA_II = ee.List([89, 98, 68, 30, 37, 100]);
var CND_II = ee.List([95,	98,	89,	77,	79, 100]);
var CNB_II = ee.List([92,	98,	79,	55,	60, 100]);

var CNA_I = ee.List([76, 94,	48,	15,	20, 100]);
var CNB_I = ee.List([81, 94, 62,	35,	40, 100]);
var CND_I = ee.List([87, 94, 76, 59, 62, 100]);

var CNA_III = ee.List([96,	99,	84,	50,	57, 100]);
var CNB_III = ee.List([97, 99, 91,	74,	78, 100]);
var CND_III = ee.List([98, 99, 96, 89, 91, 100]);

/*
// MINIMUM CURVE NUMBERS
var CNA_II = ee.List([86, 98, 68, 30, 30, 100]);
var CNB_II = ee.List([91, 98, 79, 55, 56, 100]);
var CND_II = ee.List([94,98, 89, 77, 77, 100]);

var CNA_I = ee.List([72, 94, 48, 15, 15, 100]);
var CNB_I = ee.List([80, 94, 62, 35, 36, 100]);
var CND_I = ee.List([85, 94, 76, 59, 59, 100]);

var CNA_III = ee.List([94, 99, 84, 50, 50, 100]);
var CNB_III = ee.List([97, 99, 91, 74, 74, 100]);
var CND_III = ee.List([98, 99, 96, 96, 96, 100]);


/*
// MAXIMUM CURVE NUMBERS
var CNA_II = ee.List([92, 98, 68, 30, 43, 100]);
var CNB_II = ee.List([94, 98, 79, 55, 65, 100]);
var CND_II = ee.List([96, 98, 89, 77, 82, 100]);

var CNA_I = ee.List([81, 94, 48, 15, 25, 100]);
var CNB_I = ee.List([85, 94, 62, 35, 45, 100]);
var CND_I = ee.List([89, 94, 76, 59, 66, 100]);
				
var CNA_III = ee.List([97, 99, 84, 50, 63, 100]);
var CNB_III = ee.List([98, 99, 91, 74, 82, 100]);
var CND_III = ee.List([99, 99, 96, 96, 92, 100]);
*/


// Combine different CNs of hydrologic soil groups into CN map
var zonesA_II = LULC.remap(land_classes, CNA_II, null, 'classification').rename('CN');
var zonesA_I = LULC.remap(land_classes, CNA_I, null, 'classification').rename('CNI');
var zonesA_III = LULC.remap(land_classes, CNA_III, null, 'classification').rename('CNIII');
var zonesA = zonesA_II.addBands(zonesA_I).addBands(zonesA_III).multiply(HSG.select('b1').eq(1));

var zonesB_II = LULC.remap(land_classes, CNB_II, null, 'classification').rename('CN');
var zonesB_I = LULC.remap(land_classes, CNB_I, null, 'classification').rename('CNI');
var zonesB_III = LULC.remap(land_classes, CNB_III, null, 'classification').rename('CNIII');
var zonesB = zonesB_II.addBands(zonesB_I).addBands(zonesB_III).multiply(HSG.select('b1').eq(2));

var zonesD_II = LULC.remap(land_classes, CND_II, null, 'classification').rename('CN');
var zonesD_I = LULC.remap(land_classes, CND_I, null, 'classification').rename('CNI');
var zonesD_III = LULC.remap(land_classes, CND_III, null, 'classification').rename('CNIII');
var zonesD = zonesD_II.addBands(zonesD_I).addBands(zonesD_III).multiply(HSG.select('b1').eq(4));

var CN = zonesA.add(zonesB).add(zonesD);

print('Final CNs based on land use:', CN)

Map.addLayer(CN.select('CN'), {min:0, max: 100},'Curve Numers (AMCII)')

///////////////////////////////////////
//           PRECIPITATION           //
///////////////////////////////////////
print('2. PRECIPITATION')

// Set beginning and end year and months
var startYear = 2001; 
var endYear = 2021;   

var startDate = ee.Date.fromYMD(startYear, 1, 1);
var endDate = ee.Date.fromYMD(endYear + 1, 1, 1);

var years = ee.List.sequence(startYear,endYear);
var months = ee.List.sequence(1, 12);

// PRECIPITATION CHIRPS

// 5-day precipitation
var chirps_pentad = ee.ImageCollection('UCSB-CHG/CHIRPS/PENTAD')
                  .filter(ee.Filter.date(startDate,endDate));

// Daily precipitation
var chirps_daily = ee.ImageCollection('UCSB-CHG/CHIRPS/DAILY')
                  .filter(ee.Filter.date(startDate,endDate));

// Select precipitation band
var precipitationPentad = chirps_pentad.select('precipitation');
var preciptationDaily = chirps_daily.select('precipitation');

print('5-day precipitation dataset', precipitationPentad);

var visPrecip = {
  min:0,
  max: 2000,
  palette: ['#ffffcc','#a1dab4','#41b6c4','#2c7fb8','#253494']
}

// Annual precipitation from 5-day precipitation
var annualPrecip = ee.ImageCollection.fromImages(
 years.map(function (year) {
  var annual = precipitationPentad
        .filter(ee.Filter.calendarRange(year, year, 'year'))
        .sum();
 return annual
 .set('year', year)
 .set('system:time_start', ee.Date.fromYMD(year, 1, 1));
}));


///////////////////////////////////////
//              RETENTION            //
///////////////////////////////////////

/*
Here retention is calculated per AMC condition. Conditions are retreived from Mishra et al. 2013.
Formula for retention (mm): S = 25.4(1000/CN - 10)

  AMCII   if 13mm <= P <= 28
  AMCI    if P < 13 mm
  AMCIII  if P > 28 mm
  
*/

// Function for retention
var addRetention = function(image){
var retention = image.expression(
  "(P < 13) ? (25400/CNI - 254) : (P > 28) ? (25400/CNIII - 254) : (25400/CNII - 254)",{
          'P'     : image.select('precipitation'),
          'CNI'   : CN.select('CNI'),
          'CNII'  : CN.select('CN'),
          'CNIII' : CN.select('CNIII')
  }).rename('S');
  return image.addBands(retention);
};

// Add retention band to 5-day precipitation results from 2001-2020
var precipitationPentad = precipitationPentad.map(addRetention);

// Visualisation
var visRetention = {
  bands: ['S'],
  min: 0,
  max: 600
};

///////////////////////////////////////
//              RUNOFF               //
///////////////////////////////////////

print('3. RUNOFF');

/* Runoff using SCS-CN equation
   Q = (P - Ia)**2 / (P - Ia + S)
   If Ia = 0.2S -->  Q = (P - 0.2*S) ** 2 / (P + 0.8*S) [mm]
   Q = 0 for P < 0.2S
*/

// Add initial abstraction to precipitation. Ia = 0.2S. Adjust to test sensitivitiy.
var Ia_ratio = 0.2;

// Add Ia as band onto precipitation & retention dataset.
var addIa = function(image){
  var Ia = image.expression(
    "S * ratio ", {
      'S'     : image.select('S'),
      'ratio' : Ia_ratio
    }).rename('Ia') ;
    return image.addBands(Ia);
};

var precipitationPentad = precipitationPentad.map(addIa);

// Runoff function to map runoff onto precipitation.
var addRunoff = function(image){
  var runoff = image.expression(
    "(P <= 0.2*S) ? 0 : ((P - Ia)**2 / (P - Ia + S))",{
    'P' : image.select('precipitation'),
    'Ia': image.select('Ia'),
    'S' : image.select('S')
  }).rename('Q')
  return image.addBands(runoff);
};

// Apply function to daily preciptation collection. 
// 'WBcomp' will now contain precipitaiton, runoff, and retention.
var WBcomp = precipitationPentad.map(addRunoff)

var runoffDaily = WBcomp.select('Q');
print('Daily runoff collection:', runoffDaily);

///////////////////////////////////////
//           Evaporation             //
///////////////////////////////////////
print('4. EVAPORATION');

// MODIS products avilable from 2001 to present day: https://mygeoblog.com/2017/05/18/annual-evapotranspiration/

// Get ET from MODIS 
var MODIS = ee.ImageCollection('MODIS/006/MOD16A2')
              .filter(ee.Filter.date(startDate,endDate));
              
var ET = MODIS.select('ET','PET');
print('MODIS daily ET collection:', ET)

var mod16 = MODIS.select('ET', 'PET').filterBounds(classified.geometry());

// Monthly ET from 8-day ET
var mod16monthly = ee.ImageCollection.fromImages(
  years.map(function (y) {
    return months.map(function(m) {
      var w = mod16.filter(ee.Filter.calendarRange(y, y, 'year'))
                    .filter(ee.Filter.calendarRange(m, m, 'month'))
                    .sum()
                    .multiply(0.1);
      return w.set('year', y)
              .set('month', m)
              .set('system:time_start', ee.Date.fromYMD(y, m, 1));
    });
  }).flatten()
); 

// Distinguish rainy season from dry season based off precipitation patterns. 
var rain = ee.List([4, 5, 6, 9, 10]);         // rainy season months
var dry = ee.List([1, 2, 3, 7, 8, 11, 12]);   // dry season months

// Select potential ET for rainy seasons.
var rainyET = mod16monthly.filter(ee.Filter.inList('month', rain)).select(['PET'],['ET']);

// Select actual ET for dry seasons.
var dryET = mod16monthly.filter(ee.Filter.inList('month', dry)).select(['ET'],['ET']);

// Merge monthly PET and ET to create new monthly dataset of ET.
var monthlyET = ee.ImageCollection(rainyET.merge(dryET));

// Create annual ET collection for time period using PET and ET.
var annualET = ee.ImageCollection.fromImages(
 years.map(function (year) {
  var annual = ET
        .filter(ee.Filter.calendarRange(year, year, 'year'))
        .sum()
        .multiply(0.1)
 return annual
 .set('year', year)
 .set('system:time_start', ee.Date.fromYMD(year, 1, 1));
}));



var evapotranspirationVis = {
min: 0,
max: 1300,
palette: '000000, 0000FF, FDFF92, FF2700, FF00E7'
};

//Map.addLayer(annualET.select('ET'), evapotranspirationVis,'ET before processing')

// Add land cover bands to ET ('classified' or 'LULC')
var monthlyET = monthlyET.map(function (image) {
  return image.addBands(classified);
});

// print('Monthly ET collection', monthlyET)

var unprocessedET = monthlyET;

/*
!! Some unusable scenes exist for different land cover classes. Uncomment depending on which
land classification year is selected.
*/

// Unusable scenes for 1986:
// var monthlyET = monthlyET.filter(ee.Filter.inList('system:index', ['1_104','2_43', '2_103','2_150']).not());
        
// Unusable scenes for 2003:
// var monthlyET = monthlyET.filter(ee.Filter.inList('system:index', ['1_104','1_164', '1_212', '1_236']).not());        

// Unusable scenes for 2013:
//var monthlyET = monthlyET.filter(ee.Filter.inList('system:index', ['2_43','2_103']).not());

var monthlyET = monthlyET.filter(ee.Filter.inList('system:index', ['1_113','1_128','1_140','1_152','1_176', '1_212', '1_233','2_6', '2_18', '2_43','2_54', '2_55', '2_90', '2_102', '2_115', '2_139', '2_150', '2_151', '2_175', '2_187' , '2_199', '2_222', '2_234']).not());

/////////////////////////////////////////////////////////
// CALCULATE MEAN ET FOR DIFFERENT LAND USE CLASSES   ///
////////////////////////////////////////////////////////

var evapotranspirationVismonth = {
min: 0,
max: 100,
palette: '000000, 0000FF, FDFF92, FF2700, FF00E7'
};

// Group ET based on classification
var TEST_grouping = ee.List(monthlyET.first().reduceRegion({
    reducer: ee.Reducer.mean().group({
    groupField: 1,
    groupName: 'classification'
    }),
  geometry: classified.geometry(),
  scale: 30,
  maxPixels: 1e8
  }).get('groups'));  

var TESTmyList = ee.List.sequence(0, 5);

var TESTmeanET = function(number) {
    return ee.Dictionary(ee.List(TEST_grouping.get(number))).get('mean');
    };
    
var TESTETu_list = TESTmyList.map(TESTmeanET).flatten();

// Function to map updated ET over image collection
var ETupdated = function(image){
  // Group ET per land use class
  var meanETmonth = ee.List(image.reduceRegion({
      reducer: ee.Reducer.mean().group({
      groupField: 1,
      groupName: 'classification'
      }),
    geometry: classified.geometry(),
    scale: 30,
    maxPixels: 1e8
    }).get('groups'));  
      // Generate a list of numbers from 0 to 5 (6 total classes).
      var myList = ee.List.sequence(0, 5);
      // Define a function to create a list of average ET values per landclass.
      var meanET = function(number) {
      return ee.Dictionary(ee.List(meanETmonth.get(number))).get('mean');
      };
      // Apply function to each item (land cover class) in the list by using the map() function.
      var ETu_list = myList.map(meanET).flatten();
    
        // Create new function to add mean ET to specific land cover classes.
    // Create new ET map based on the mean values per land cover class.
    var ETreplace = image.remap(land_classes, ETu_list, null, 'classification')
    .rename('ETu');
    
        // Add ET values for masked areas: For each pixel in each band of 'input', 
        // if the corresponding pixel in 'test' is nonzero, output the corresponding 
        // pixel in value, otherwise output the input pixel.
    //return image.unmask(ETreplace.select('ETu'))  
    return image.addBands(ETreplace).toFloat()
};

// Rename updated ETu to ET
var monthlyET = monthlyET.map(ETupdated).select(['ETu'],['ET']);

// Annual modified ET
var annualET = ee.ImageCollection.fromImages(
 years.map(function (year) {
  var annual = monthlyET
        .filter(ee.Filter.calendarRange(year, year, 'year'))
        .sum();
 return annual
 .set('year', year)
 .set('system:time_start', ee.Date.fromYMD(year, 1, 1));
}));

print('Monthly ET collection (final:)', monthlyET)

// UNPROCESSED ET STATISTICS (FOR STD.DEV RETREIVAL)

// Annual unprocessed ET
var unprocessedAnnualET = ee.ImageCollection.fromImages(
 years.map(function (year) {
  var annual = unprocessedET
        .filter(ee.Filter.calendarRange(year, year, 'year'))
        .sum();
 return annual
 .set('year', year)
 .set('system:time_start', ee.Date.fromYMD(year, 1, 1));
}));

var unprocessedAnnualET = unprocessedAnnualET.select('ET').mean()//.clip(geometry);

var unproAddedclassET = unprocessedAnnualET.addBands(classified);

var zonalStatsET_unprocessed = ee.List(unproAddedclassET.reduceRegion({
      reducer: ee.Reducer.mean().group({
      groupField: 1,
      groupName: 'classification'
      }),
    geometry: classified.geometry(),
    scale: 30,
    maxPixels: 1e8
    }).get('groups'));

var zonalStatsET_unprocessed_list = land_classes.map(function(number) {
    return ee.Dictionary(ee.List(zonalStatsET_unprocessed.get(number))).get('mean');
    });
    
print('ET per land class (unprocessed):', zonalStatsET_unprocessed_list);

///////////////////////////////////////
//             RECHARGE              //
///////////////////////////////////////

print('5. RECHARGE')

// Monthly water balance: recharge = P - Q - ETa

// Daily recharge function without ET
var addRecharge = function(image){
  var recharge = image.expression(
  '(P - Q )',{
    'P': image.select('precipitation'),
    'Q': image.select('Q')
  }).rename('recharge');
  return image.addBands(recharge);
};

// Apply function to water balance components
var recharge = WBcomp.map(addRecharge);

var first_GWR = recharge.first()

var areaImage = first_GWR.multiply(ee.Image.pixelArea())
var area = areaImage.reduceRegion({
  reducer: ee.Reducer.sum(),
  geometry: geometry,
  scale: 500,
  maxPixels: 1e10
  })

// Annual and monthly recharge collections

// Function for monthly recharge without ET
var monthlyRecharge =  ee.ImageCollection.fromImages(
  years.map(function (y) {
    return months.map(function(m) {
      var w = recharge.filter(ee.Filter.calendarRange(y, y, 'year'))
                    .filter(ee.Filter.calendarRange(m, m, 'month'))
                    .sum();
      return w.set('year', y)
              .set('month', m)
              .set('system:time_start', ee.Date.fromYMD(y, m, 1));
    });
  }).flatten());


// Join annual water balance components to ET using 'inner join'. This is done 
// because all components need to be in one image collection as different bands.
var filter = ee.Filter.equals({
  leftField: 'system:time_start',
  rightField: 'system:time_start'
});

var simpleJoin = ee.Join.inner();

var innerJoinMonthly = ee.ImageCollection(simpleJoin.apply(monthlyRecharge, monthlyET, filter));

var joinedMonthly = innerJoinMonthly.map(function(feature){
  return ee.Image.cat(feature.get('primary'), feature.get('secondary'));
});

// Recharge function with ET, includes ET in recharge estimation
var addET = function(image){
  var rechargeET = image.expression(
  '(P - Q - ET)',{
    'P': image.select('precipitation'),
    'Q': image.select('Q'),
    'ET': image.select('ET')
  }).rename('recharge_final')
  return image.addBands(rechargeET);
};


// Apply function to monthly recharge
var monthlyWB = joinedMonthly.map(addET);

print('Monthly water balance collection:', monthlyWB)


////////////////////////////////////////
/////     Results visualisation     ////
///////////////////////////////////////

// Annual water balance components
var annualWB = ee.ImageCollection.fromImages(
 years.map(function (year) {
  var annual = monthlyWB
        .filter(ee.Filter.calendarRange(year, year, 'year'))
        .sum();
 return annual
 .set('year', year)
 .set('system:time_start', ee.Date.fromYMD(year, 1, 1));
}));

print('Annual WB collection:', annualWB)

// Annual runoff for visualisation
var annualRunoff = annualWB.select('Q').mean().clip(geometry);

// Visualisation
var runoffVis = {
  min: 0,
  max: 945,
  palette:['#A1D4FF','#63B6FC','#3898EA','#197ACD','#065AA3','#003C6F'] // blues
};

Map.addLayer(annualRunoff, runoffVis, 'Mean annual runoff (mm)');

// ANNUAL ET STATISTICS 
var annualET = annualWB.select('ET').mean().clip(geometry);

var addedclassET = annualET.addBands(classified)

var zonalStatsET = ee.List(addedclassET.reduceRegion({
      reducer: ee.Reducer.mean().group({
      groupField: 1,
      groupName: 'classification'
      }),
    geometry: geometry,
    scale: 30,
    maxPixels: 1e8
    }).get('groups'));

var zonalStatsET_list = land_classes.map(function(number) {
    return ee.Dictionary(ee.List(zonalStatsET.get(number))).get('mean');
    });
    
    
print('6. WATER BALANCE RESULTS: ')
print('ET per land class (mm):', zonalStatsET_list);

Map.addLayer(annualET, evapotranspirationVis, 'Mean annual ET (mm)')

// ANNUAL Q STATISTICS 
var annualQ = annualWB.select('Q').mean().clip(geometry);

var addedclassQ = annualQ.addBands(classified)

var zonalStatsQ = ee.List(addedclassQ.reduceRegion({
      reducer: ee.Reducer.mean().group({
      groupField: 1,
      groupName: 'classification'
      }),
    geometry: geometry,
    scale: 30,
    maxPixels: 1e8
    }).get('groups'));

var zonalStatsQ_list = land_classes.map(function(number) {
    return ee.Dictionary(ee.List(zonalStatsQ.get(number))).get('mean');
    });
    
print('Q per land class (mm):', zonalStatsQ_list);

//  ANNUAL RECHARGE STATISTICS
var annualRecharge = annualWB.select('recharge_final').mean().clip(geometry);

var addedclassGWR = annualRecharge.addBands(classified);

var zonalStatsGWR = ee.List(addedclassGWR.reduceRegion({
      reducer: ee.Reducer.mean().group({
      groupField: 1,
      groupName: 'classification'
      }),
    geometry: geometry,
    scale: 30,
    maxPixels: 1e8
    }).get('groups'));

var zonalStatsGWR_list = land_classes.map(function(number) {
    return ee.Dictionary(ee.List(zonalStatsGWR.get(number))).get('mean');
    });
    
print('Recharge per land class (mm):', zonalStatsGWR_list);


// Annual precipitation
var annualPrecip = annualWB.select('precipitation').mean().clip(geometry);

/////////////////////////////////////
// Mean components of WB          //
////////////////////////////////////

var annualET_mean = annualET.reduceRegion({
  reducer: ee.Reducer.mean(),
  scale: 30,
  geometry: geometry,
  maxPixels: 1e9});

var annualPrecip_mean = annualPrecip.reduceRegion({
  reducer: ee.Reducer.mean(),
  scale: 30,
  geometry: geometry,
  maxPixels: 1e9});
  
var annualRunoff_mean = annualRunoff.reduceRegion({
  reducer: ee.Reducer.mean(),
  scale: 30,
  geometry: geometry,
  maxPixels: 1e9});

var annualRecharge_mean = annualRecharge.reduceRegion({
  reducer: ee.Reducer.mean(),
  scale: 30,
  geometry: geometry,
  maxPixels: 1e9});

print('Mean annual precipitation (mm):', annualPrecip_mean.get('precipitation'));
print('Mean annual runoff (mm):', annualRunoff_mean.get('Q'));
print('Mean annual recharge (mm):', annualRecharge_mean.get('recharge_final'));
print('Mean annual ET (mm):', annualET_mean.get('ET'));


//////////////////////////////////////////////////////////////////////////////
//          Export images, creating charts specifying scale and region.     //
//////////////////////////////////////////////////////////////////////////////

Export.image.toDrive({
  image: annualET,
  description: 'annualET',
  scale: 100,
  region: geometry
});

Export.image.toDrive({
  image: annualRecharge,
  description: 'annualRecharge',
  scale: 100,
  region: geometry
});


// ANNUAL RECHARGE
var titleGWR = {
 title: 'Annual recharge (mm)',
 hAxis: {title: 'Year'},
 vAxis: {title: 'Groundwater recharge (mm)'},
};
 
var chartGWR = ui.Chart.image.seriesByRegion({
 imageCollection: annualWB,
 regions: geometry,
 reducer: ee.Reducer.mean(),
 band: 'recharge_final',
 scale: 500,
 seriesProperty: 'SITE'
}).setOptions(titleGWR)
 .setChartType('ColumnChart');
 
print(chartGWR);

// MONTHLY RECHARGE CHART

var meanMonthlyWB =  ee.ImageCollection.fromImages(
  months.map(function (m) {
    var w = monthlyWB.filter(ee.Filter.eq('month', m)).mean();
    return w.set('month', m)
            .set('system:time_start',(ee.Date.fromYMD(1, m, 1)).format('MMMMMM'));
  }).flatten()
);

var titleMonthlyGWR = {
  title: 'Long-term avg. monthly recharge (mm)',
  hAxis: {title: 'Month'},
  vAxis: {title: 'Groundwater recharge (mm)'},};

var chartMonthlyGWR = ui.Chart.image.seriesByRegion({
  imageCollection: meanMonthlyWB, 
  regions: geometry,
  reducer: ee.Reducer.mean(),
  band: 'recharge_final',
  scale: 10,
  seriesProperty: 'SITE'})
  .setOptions(titleMonthlyGWR)
  .setChartType('ColumnChart');

print(chartMonthlyGWR);

// MONTHLY PRECIPITATION CHART

var titleMonthlyP = {
  title: 'Long-term avg. monthly precipitation (mm)',
  hAxis: {title: 'Month'},
  vAxis: {title: 'Precipitation (mm)'},};

var chartMonthlyP = ui.Chart.image.seriesByRegion({
  imageCollection: meanMonthlyWB, 
  regions: geometry,
  reducer: ee.Reducer.mean(),
  band: 'precipitation',
  scale: 2500,
  seriesProperty: 'SITE'})
  .setOptions(titleMonthlyP)
  .setChartType('ColumnChart');

print(chartMonthlyP);

//*****************************************************************
// Annual Precipitation
var titleAnnualP = {
  title: 'Annual precipitation (mm)',
  hAxis: {title: 'year'},
  vAxis: {title: 'Precipitation (mm)'},};

var chartAnnualP = ui.Chart.image.seriesByRegion({
  imageCollection: annualWB, 
  regions: geometry,
  reducer: ee.Reducer.mean(),
  band: 'precipitation',
  scale: 2500,
  seriesProperty: 'SITE'})
  .setOptions(titleAnnualP)
  .setChartType('ColumnChart');

print(chartAnnualP);

// Annual Evapotranspiration
var titleET = {
 title: 'Annual evapotranspiration (mm)',
 hAxis: {title: 'Year'},
 vAxis: {title: 'ET (mm)'},
};
 
var chartET = ui.Chart.image.seriesByRegion({
 imageCollection: annualWB,
 regions: geometry,
 reducer: ee.Reducer.mean(),
 band: 'ET',
 scale: 500,
 seriesProperty: 'SITE'
}).setOptions(titleET)
 .setChartType('ColumnChart');
 
print(chartET);


//****************************************************************


// MONTHLY EVAPOTRANSPIRATION CHART
var titleMonthlyET = {
  title: 'Long-term avg. monthly evapotranspiration (mm)',
  hAxis: {title: 'Month'},
  vAxis: {title: 'ET (mm)'},};

var chartMonthlyET = ui.Chart.image.seriesByRegion({
  imageCollection: meanMonthlyWB, 
  regions: geometry,
  reducer: ee.Reducer.mean(),
  band: 'ET',
  scale: 2500,
  seriesProperty: 'SITE'})
  .setOptions(titleMonthlyET)
  .setChartType('ColumnChart');

print(chartMonthlyET);

// MONTHLY RUNOFF CHART
var titleMonthlyQ = {
  title: 'Long-term avg. monthly runoff (mm)',
  hAxis: {title: 'Month'},
  vAxis: {title: 'Runoff (mm)'},};

var chartMonthlyQ = ui.Chart.image.seriesByRegion({
  imageCollection: meanMonthlyWB, 
  regions: geometry,
  reducer: ee.Reducer.mean(),
  band: 'Q',
  scale: 2500,
  seriesProperty: 'SITE'})
  .setOptions(titleMonthlyQ)
  .setChartType('ColumnChart');

print(chartMonthlyQ);

// Find min and max  recharge for visualisation

var rechargeMax = annualRecharge.reduceRegion({
  reducer: ee.Reducer.max(),
  geometry: geometry,
  scale: 10,maxPixels: 1e9}).getNumber('recharge_final');
  
var rechargeMin = annualRecharge.reduceRegion({
  reducer: ee.Reducer.min(),
  scale: 10,
  geometry: geometry,
  maxPixels: 1e9}).getNumber('recharge_final');

var rechargeVis = {
  min: -614, 
  max: 782, 
  palette: ['#FF8C00', '#FFFF00', '#32CD32', '#228B22', '#4169E1', '#000080']};

Map.addLayer(annualRecharge, rechargeVis,'Recharge (mm)')

//Map.addLayer(collectionPts, 'Collection points')

// LEGEND FOR ANNUAL RECHARGE

var legendGWR = ui.Panel({
  style: {
    position: 'bottom-left',
    padding: '8px 15px'
  }
});
 
// Create legend title
var legendTitleGWR = ui.Label({
  value: 'Recharge (mm)',
  style: {
    fontWeight: 'bold',
    fontSize: '18px',
    margin: '0 0 4px 0',
    padding: '0'
    }
});

legendGWR.add(legendTitleGWR);

// Create the legend image
var lonGWR = ee.Image.pixelLonLat().select('latitude');
var gradientGWR = lonGWR.multiply((rechargeVis.max-rechargeVis.min)/100.0).add(rechargeVis.min);
var legendImageGWR = gradientGWR.visualize(rechargeVis);
 
// create text on top of legend
var panelGWR = ui.Panel({
widgets: [
ui.Label(rechargeVis['max'])
],
});
 
legendGWR.add(panelGWR);

var thumbnailGWR = ui.Thumbnail({
image: legendImageGWR,
params: {bbox:'0,0,10,100', dimensions:'8x150'},
style: {padding: '1px', position: 'bottom-center'}
});
 
// Add the thumbnail to the legend
legendGWR.add(thumbnailGWR);
 
//Create text on top of legend
var panelGWR = ui.Panel({
widgets: [
ui.Label(rechargeVis['min'])
],
});
 
legendGWR.add(panelGWR);
print(legendGWR)

// Legend runoff
var legendQ = ui.Panel({
  style: {
    position: 'bottom-left',
    padding: '8px 15px'
  }
});
 
// Create legend title
var legendTitleQ = ui.Label({
  value: 'Runoff (mm)',
  style: {
    fontWeight: 'bold',
    fontSize: '18px',
    margin: '0 0 4px 0',
    padding: '0'
    }
});

legendQ.add(legendTitleQ);

// Create the legend image
var lonQ = ee.Image.pixelLonLat().select('latitude');
var gradientQ = lonQ.multiply((runoffVis.max-runoffVis.min)/100.0).add(runoffVis.min);
var legendImageQ = gradientQ.visualize(runoffVis);
 
// create text on top of legend
var panelQ = ui.Panel({
widgets: [
ui.Label(runoffVis['max'])
],
});
 
legendQ.add(panelQ);

var thumbnailQ = ui.Thumbnail({
image: legendImageQ,
params: {bbox:'0,0,10,100', dimensions:'8x150'},
style: {padding: '1px', position: 'bottom-center'}
});
 
// Add the thumbnail to the legend
legendQ.add(thumbnailQ);
 
//Create text on top of legend
var panelQ = ui.Panel({
widgets: [
ui.Label(runoffVis['min'])
],
});
 
legendQ.add(panelQ);
print(legendQ)

print('finished')

// MONTHLY RUNOFF


var chartMonthlyQ = ui.Chart.image.seriesByRegion(meanMonthlyWB.select('precipitation'), 
        meanMonthlyWB.select('S'), ee.Reducer.mean(), 2500, 'system:time_start')
        .setOptions(titleMonthlyQ)  
        .setChartType('ColumnChart');

///////////////////////////////////////////////////////////////////////////////////////
//                                      END                                          //
///////////////////////////////////////////////////////////////////////////////////////