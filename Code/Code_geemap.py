

######################
#              CURVE NUMBERS             #
######################

print('RESULTS PANEL')
print('1. CURVE NUMBER ESTIMATES')

# Use USDA tables to retreive curve numbers for specific group combinations.

Below are CNs for normal AMC conditions.

HSG raster notation      1     2     4
(0)Urban semipervious	74	  83.5	90
(1)Urban impervious	    98	  98	98
(2)Bare	                68	  79	89
(3)Dense vegetation	    30    55	77
(4)Sparse vegetation	36.75 60    79.25
(5)Water                100   100   100


# Create lists for Curve Number selection based on land use class
land_classes = ee.List([0, 1, 2, 3, 4, 5])

#
Average, minimum and maximum curve numbers per land use class are given below.
Select the average curve numbers for runoff estimates. To investigate CN ranges
select minimum or maximum curve numbers.
#

# AVERAGE CURVE NUMBERS
CNA_II = ee.List([89, 98, 68, 30, 37, 100])
CND_II = ee.List([95,	98,	89,	77,	79, 100])
CNB_II = ee.List([92,	98,	79,	55,	60, 100])

CNA_I = ee.List([76, 94,	48,	15,	20, 100])
CNB_I = ee.List([81, 94, 62,	35,	40, 100])
CND_I = ee.List([87, 94, 76, 59, 62, 100])

CNA_III = ee.List([96,	99,	84,	50,	57, 100])
CNB_III = ee.List([97, 99, 91,	74,	78, 100])
CND_III = ee.List([98, 99, 96, 89, 91, 100])

#
# MINIMUM CURVE NUMBERS
CNA_II = ee.List([86, 98, 68, 30, 30, 100])
CNB_II = ee.List([91, 98, 79, 55, 56, 100])
CND_II = ee.List([94,98, 89, 77, 77, 100])

CNA_I = ee.List([72, 94, 48, 15, 15, 100])
CNB_I = ee.List([80, 94, 62, 35, 36, 100])
CND_I = ee.List([85, 94, 76, 59, 59, 100])

CNA_III = ee.List([94, 99, 84, 50, 50, 100])
CNB_III = ee.List([97, 99, 91, 74, 74, 100])
CND_III = ee.List([98, 99, 96, 96, 96, 100])


#
# MAXIMUM CURVE NUMBERS
CNA_II = ee.List([92, 98, 68, 30, 43, 100])
CNB_II = ee.List([94, 98, 79, 55, 65, 100])
CND_II = ee.List([96, 98, 89, 77, 82, 100])

CNA_I = ee.List([81, 94, 48, 15, 25, 100])
CNB_I = ee.List([85, 94, 62, 35, 45, 100])
CND_I = ee.List([89, 94, 76, 59, 66, 100])

CNA_III = ee.List([97, 99, 84, 50, 63, 100])
CNB_III = ee.List([98, 99, 91, 74, 82, 100])
CND_III = ee.List([99, 99, 96, 96, 92, 100])
#


# Combine different CNs of hydrologic soil groups into CN map
zonesA_II = LULC.remap(land_classes, CNA_II, None, 'classification').rename('CN')
zonesA_I = LULC.remap(land_classes, CNA_I, None, 'classification').rename('CNI')
zonesA_III = LULC.remap(land_classes, CNA_III, None, 'classification').rename('CNIII')
zonesA = zonesA_II.addBands(zonesA_I).addBands(zonesA_III).multiply(HSG.select('b1').eq(1))

zonesB_II = LULC.remap(land_classes, CNB_II, None, 'classification').rename('CN')
zonesB_I = LULC.remap(land_classes, CNB_I, None, 'classification').rename('CNI')
zonesB_III = LULC.remap(land_classes, CNB_III, None, 'classification').rename('CNIII')
zonesB = zonesB_II.addBands(zonesB_I).addBands(zonesB_III).multiply(HSG.select('b1').eq(2))

zonesD_II = LULC.remap(land_classes, CND_II, None, 'classification').rename('CN')
zonesD_I = LULC.remap(land_classes, CND_I, None, 'classification').rename('CNI')
zonesD_III = LULC.remap(land_classes, CND_III, None, 'classification').rename('CNIII')
zonesD = zonesD_II.addBands(zonesD_I).addBands(zonesD_III).multiply(HSG.select('b1').eq(4))

CN = zonesA.add(zonesB).add(zonesD)

print('Final CNs based on land use:', CN)

Map.addLayer(CN.select('CN'), {'min':0, 'max': 100},'Curve Numers (AMCII)')

###################/
#           PRECIPITATION           #
###################/
print('2. PRECIPITATION')

# Set beginning and end year and months
startYear = 2001
endYear = 2021

startDate = ee.Date.fromYMD(startYear, 1, 1)
endDate = ee.Date.fromYMD(endYear + 1, 1, 1)

years = ee.List.sequence(startYear,endYear)
months = ee.List.sequence(1, 12)

# PRECIPITATION CHIRPS

# 5-day precipitation
chirps_pentad = ee.ImageCollection('UCSB-CHG/CHIRPS/PENTAD') \
                  .filter(ee.Filter.date(startDate,endDate))

# Daily precipitation
chirps_daily = ee.ImageCollection('UCSB-CHG/CHIRPS/DAILY') \
                  .filter(ee.Filter.date(startDate,endDate))

# Select precipitation band
precipitationPentad = chirps_pentad.select('precipitation')
preciptationDaily = chirps_daily.select('precipitation')

print('5-day precipitation dataset', precipitationPentad)

visPrecip = {
  'min':0,
  'max': 2000,
  'palette': ['#ffffcc','#a1dab4','#41b6c4','#2c7fb8','#253494']
}

# Annual precipitation from 5-day precipitation
annualPrecip = ee.ImageCollection.fromImages(

def func_pbo (year):
  annual = precipitationPentad \
        .filter(ee.Filter.calendarRange(year, year, 'year')) \
        .sum()
 return annual \
 .set('year', year) \
 .set('system:time_start', ee.Date.fromYMD(year, 1, 1))

 years.map(func_pbo
))






))


###################/
#              RETENTION            #
###################/

#
Here retention is calculated per AMC condition. Conditions are retreived from Mishra et al. 2013.
'Formula for retention (mm)': S = 25.4(1000/CN - 10)

  AMCII   if 13mm <= P <= 28
  AMCI    if P < 13 mm
  AMCIII  if P > 28 mm

#

# Function for retention
def addRetention(image):
retention = image.expression(
  "(P < 13) ? (25400/CNI - 254) :  '(P > 28) ? (25400/CNIII - 254)': (25400/CNII - 254)",{
          'P'     : image.select('precipitation'),
          'CNI'   : CN.select('CNI'),
          'CNII'  : CN.select('CN'),
          'CNIII' : CN.select('CNIII')
  }).rename('S')
  return image.addBands(retention)


# Add retention band to 5-day precipitation results from 2001-2020
precipitationPentad = precipitationPentad.map(addRetention)

# Visualisation
visRetention = {
  'bands': ['S'],
  'min': 0,
  'max': 600
}

###################/
#              RUNOFF               #
###################/

print('3. RUNOFF')

# Runoff using SCS-CN equation
   Q = (P - Ia)**2 / (P - Ia + S)
   If Ia = 0.2S -->  Q = (P - 0.2*S) ** 2 / (P + 0.8*S) [mm]
   Q = 0 for P < 0.2S
#

# Add initial abstraction to precipitation. Ia = 0.2S. Adjust to test sensitivitiy.
Ia_ratio = 0.2

# Add Ia as band onto precipitation & retention dataset.
def addIa(image):
  Ia = image.expression(
    "S * ratio ", {
      'S'     : image.select('S'),
      'ratio' : Ia_ratio
    }).rename('Ia') 
    return image.addBands(Ia)


precipitationPentad = precipitationPentad.map(addIa)

# Runoff function to map runoff onto precipitation.
def addRunoff(image):
  runoff = image.expression(
    "(P <= 0.2*S) ? 0 : ((P - Ia)**2 / (P - Ia + S))",{
    'P' : image.select('precipitation'),
    'Ia': image.select('Ia'),
    'S' : image.select('S')
  }).rename('Q')
  return image.addBands(runoff)


# Apply function to daily preciptation collection.
# 'WBcomp' will now contain precipitaiton, runoff, and retention.
WBcomp = precipitationPentad.map(addRunoff)

runoffDaily = WBcomp.select('Q')
print('Daily runoff collection:', runoffDaily)

###################/
#           Evaporation             #
###################/
print('4. EVAPORATION')

# MODIS products avilable from 2001 to present day: https:#mygeoblog.com/2017/05/18/annual-evapotranspiration/

# Get ET from MODIS
MODIS = ee.ImageCollection('MODIS/006/MOD16A2') \
              .filter(ee.Filter.date(startDate,endDate))

ET = MODIS.select('ET','PET')
print('MODIS daily ET collection:', ET)

mod16 = MODIS.select('ET', 'PET').filterBounds(classified.geometry())

# Monthly ET from 8-day ET
mod16monthly = ee.ImageCollection.fromImages(

def func_yxa (y):
    return months.map(function(m) {
      w = mod16.filter(ee.Filter.calendarRange(y, y, 'year')) \
                    .filter(ee.Filter.calendarRange(m, m, 'month')) \
                    .sum() \
                    .multiply(0.1)
      return w.set('year', y) \
              .set('month', m) \
              .set('system:time_start', ee.Date.fromYMD(y, m, 1))
    })

  years.map(func_yxa
).flatten()









).flatten()
)

# Distinguish rainy season from dry season based off precipitation patterns.
rain = ee.List([4, 5, 6, 9, 10]);         # rainy season months
dry = ee.List([1, 2, 3, 7, 8, 11, 12]);   # dry season months

# Select potential ET for rainy seasons.
rainyET = mod16monthly.filter(ee.Filter.inList('month', rain)).select(['PET'],['ET'])

# Select actual ET for dry seasons.
dryET = mod16monthly.filter(ee.Filter.inList('month', dry)).select(['ET'],['ET'])

# Merge monthly PET and ET to create new monthly dataset of ET.
monthlyET = ee.ImageCollection(rainyET.merge(dryET))

# Create annual ET collection for time period using PET and ET.
annualET = ee.ImageCollection.fromImages(

def func_rtr (year):
  annual = ET \
        .filter(ee.Filter.calendarRange(year, year, 'year')) \
        .sum() \
        .multiply(0.1)
 return annual \
 .set('year', year) \
 .set('system:time_start', ee.Date.fromYMD(year, 1, 1))

 years.map(func_rtr
))







))



evapotranspirationVis = {
'min': 0,
'max': 1300,
'palette': '000000, 0000FF, FDFF92, FF2700, FF00E7'
}

#Map.addLayer(annualET.select('ET'), evapotranspirationVis,'ET before processing')

# Add land cover bands to ET ('classified' or 'LULC')

def func_kny (image):
  return image.addBands(classified)

monthlyET = monthlyET.map(func_kny)




# print('Monthly ET collection', monthlyET)

unprocessedET = monthlyET

#
!! Some unusable scenes exist for different land cover classes. Uncomment depending on which
land classification year is selected.
#

# Unusable scenes for 1986:
# monthlyET = monthlyET.filter(ee.Filter.inList('system:index', ['1_104','2_43', '2_103','2_150']).Not())

# Unusable scenes for 2003:
# monthlyET = monthlyET.filter(ee.Filter.inList('system:index', ['1_104','1_164', '1_212', '1_236']).Not())

# Unusable scenes for 2013:
#monthlyET = monthlyET.filter(ee.Filter.inList('system:index', ['2_43','2_103']).Not())

monthlyET = monthlyET.filter(ee.Filter.inList('system:index', ['1_113','1_128','1_140','1_152','1_176', '1_212', '1_233','2_6', '2_18', '2_43','2_54', '2_55', '2_90', '2_102', '2_115', '2_139', '2_150', '2_151', '2_175', '2_187' , '2_199', '2_222', '2_234']).Not())

############################/
# CALCULATE MEAN ET FOR DIFFERENT LAND USE CLASSES   #/
############################

evapotranspirationVismonth = {
'min': 0,
'max': 100,
'palette': '000000, 0000FF, FDFF92, FF2700, FF00E7'
}

# Group ET based on classification
TEST_grouping = ee.List(monthlyET.first().reduceRegion({
    'reducer': ee.Reducer.mean().group({
    'groupField': 1,
    'groupName': 'classification'
    }),
  'geometry': classified.geometry(),
  'scale': 30,
  'maxPixels': 1e8
  }).get('groups'))

TESTmyList = ee.List.sequence(0, 5)

def TESTmeanET(number):
    return ee.Dictionary(ee.List(TEST_grouping.get(number))).get('mean')
    

TESTETu_list = TESTmyList.map(TESTmeanET).flatten()

# Function to map updated ET over image collection
def ETupdated(image):
  # Group ET per land use class
  meanETmonth = ee.List(image.reduceRegion({
      'reducer': ee.Reducer.mean().group({
      'groupField': 1,
      'groupName': 'classification'
      }),
    'geometry': classified.geometry(),
    'scale': 30,
    'maxPixels': 1e8
    }).get('groups'))
      # Generate a list of numbers from 0 to 5 (6 total classes).
      myList = ee.List.sequence(0, 5)
      # Define a function to create a list of average ET values per landclass.
      def meanET(number):
      return ee.Dictionary(ee.List(meanETmonth.get(number))).get('mean')
      
      # Apply function to each item (land cover class) in the list by using the map() function.
      ETu_list = myList.map(meanET).flatten()

        # Create new function to add mean ET to specific land cover classes.
    # Create new ET map based on the mean values per land cover class.
    ETreplace = image.remap(land_classes, ETu_list, None, 'classification') \
    .rename('ETu')

        # Add ET values for masked areas: For each pixel in each band of 'input',
        # if the corresponding pixel in 'test' is nonzero, output the corresponding
        # pixel in value, otherwise output the input pixel.
    #return image.unmask(ETreplace.select('ETu'))
    return image.addBands(ETreplace).toFloat()


# Rename updated ETu to ET
monthlyET = monthlyET.map(ETupdated).select(['ETu'],['ET'])

# Annual modified ET
annualET = ee.ImageCollection.fromImages(

def func_jku (year):
  annual = monthlyET \
        .filter(ee.Filter.calendarRange(year, year, 'year')) \
        .sum()
 return annual \
 .set('year', year) \
 .set('system:time_start', ee.Date.fromYMD(year, 1, 1))

 years.map(func_jku
))






))

print('Monthly ET collection (final:)', monthlyET)

# UNPROCESSED ET STATISTICS (FOR STD.DEV RETREIVAL)

# Annual unprocessed ET
unprocessedAnnualET = ee.ImageCollection.fromImages(

def func_kxa (year):
  annual = unprocessedET \
        .filter(ee.Filter.calendarRange(year, year, 'year')) \
        .sum()
 return annual \
 .set('year', year) \
 .set('system:time_start', ee.Date.fromYMD(year, 1, 1))

 years.map(func_kxa
))






))

unprocessedAnnualET = unprocessedAnnualET.select('ET').mean()#.clip(geometry)

unproAddedclassET = unprocessedAnnualET.addBands(classified)

zonalStatsET_unprocessed = ee.List(unproAddedclassET.reduceRegion({
      'reducer': ee.Reducer.mean().group({
      'groupField': 1,
      'groupName': 'classification'
      }),
    'geometry': classified.geometry(),
    'scale': 30,
    'maxPixels': 1e8
    }).get('groups'))


def func_fig(number):
    return ee.Dictionary(ee.List(zonalStatsET_unprocessed.get(number))).get('mean')

zonalStatsET_unprocessed_list = land_classes.map(func_fig)




print('ET per land class (unprocessed):', zonalStatsET_unprocessed_list)

###################/
#             RECHARGE              #
###################/

print('5. RECHARGE')

# Monthly water balance: recharge = P - Q - ETa

# Daily recharge function without ET
def addRecharge(image):
  recharge = image.expression(
  '(P - Q )',{
    'P': image.select('precipitation'),
    'Q': image.select('Q')
  }).rename('recharge')
  return image.addBands(recharge)


# Apply function to water balance components
recharge = WBcomp.map(addRecharge)

first_GWR = recharge.first()

areaImage = first_GWR.multiply(ee.Image.pixelArea())
area = areaImage.reduceRegion({
  'reducer': ee.Reducer.sum(),
  'geometry': geometry,
  'scale': 500,
  'maxPixels': 1e10
  })

# Annual and monthly recharge collections

# Function for monthly recharge without ET
monthlyRecharge =  ee.ImageCollection.fromImages(

def func_hzu (y):
    return months.map(function(m) {
      w = recharge.filter(ee.Filter.calendarRange(y, y, 'year')) \
                    .filter(ee.Filter.calendarRange(m, m, 'month')) \
                    .sum()
      return w.set('year', y) \
              .set('month', m) \
              .set('system:time_start', ee.Date.fromYMD(y, m, 1))
    })

  years.map(func_hzu
).flatten())








).flatten())


# Join annual water balance components to ET using 'inner join'. This is done
# because all components need to be in one image collection as different bands.
filter = ee.Filter.equals({
  'leftField': 'system:time_start',
  'rightField': 'system:time_start'
})

simpleJoin = ee.Join.inner()

innerJoinMonthly = ee.ImageCollection(simpleJoin.apply(monthlyRecharge, monthlyET, filter))


def func_okc(feature):
  return ee.Image.cat(feature.get('primary'), feature.get('secondary'))

joinedMonthly = innerJoinMonthly.map(func_okc)




# Recharge function with ET, includes ET in recharge estimation
def addET(image):
  rechargeET = image.expression(
  '(P - Q - ET)',{
    'P': image.select('precipitation'),
    'Q': image.select('Q'),
    'ET': image.select('ET')
  }).rename('recharge_final')
  return image.addBands(rechargeET)



# Apply function to monthly recharge
monthlyWB = joinedMonthly.map(addET)

print('Monthly water balance collection:', monthlyWB)


####################
##/     Results visualisation     ##
###################/

# Annual water balance components
annualWB = ee.ImageCollection.fromImages(

def func_hcx (year):
  annual = monthlyWB \
        .filter(ee.Filter.calendarRange(year, year, 'year')) \
        .sum()
 return annual \
 .set('year', year) \
 .set('system:time_start', ee.Date.fromYMD(year, 1, 1))

 years.map(func_hcx
))






))

print('Annual WB collection:', annualWB)

# Annual runoff for visualisation
annualRunoff = annualWB.select('Q').mean().clip(geometry)

# Visualisation
runoffVis = {
  'min': 0,
  'max': 945,
  'palette':['#A1D4FF','#63B6FC','#3898EA','#197ACD','#065AA3','#003C6F'] # blues
}

Map.addLayer(annualRunoff, runoffVis, 'Mean annual runoff (mm)')

# ANNUAL ET STATISTICS
annualET = annualWB.select('ET').mean().clip(geometry)

addedclassET = annualET.addBands(classified)

zonalStatsET = ee.List(addedclassET.reduceRegion({
      'reducer': ee.Reducer.mean().group({
      'groupField': 1,
      'groupName': 'classification'
      }),
    'geometry': geometry,
    'scale': 30,
    'maxPixels': 1e8
    }).get('groups'))


def func_vow(number):
    return ee.Dictionary(ee.List(zonalStatsET.get(number))).get('mean')

zonalStatsET_list = land_classes.map(func_vow)





print('6. WATER BALANCE RESULTS: ')
print('ET per land class (mm):', zonalStatsET_list)

Map.addLayer(annualET, evapotranspirationVis, 'Mean annual ET (mm)')

# ANNUAL Q STATISTICS
annualQ = annualWB.select('Q').mean().clip(geometry)

addedclassQ = annualQ.addBands(classified)

zonalStatsQ = ee.List(addedclassQ.reduceRegion({
      'reducer': ee.Reducer.mean().group({
      'groupField': 1,
      'groupName': 'classification'
      }),
    'geometry': geometry,
    'scale': 30,
    'maxPixels': 1e8
    }).get('groups'))


def func_iiw(number):
    return ee.Dictionary(ee.List(zonalStatsQ.get(number))).get('mean')

zonalStatsQ_list = land_classes.map(func_iiw)




print('Q per land class (mm):', zonalStatsQ_list)

#  ANNUAL RECHARGE STATISTICS
annualRecharge = annualWB.select('recharge_final').mean().clip(geometry)

addedclassGWR = annualRecharge.addBands(classified)

zonalStatsGWR = ee.List(addedclassGWR.reduceRegion({
      'reducer': ee.Reducer.mean().group({
      'groupField': 1,
      'groupName': 'classification'
      }),
    'geometry': geometry,
    'scale': 30,
    'maxPixels': 1e8
    }).get('groups'))


def func_lnr(number):
    return ee.Dictionary(ee.List(zonalStatsGWR.get(number))).get('mean')

zonalStatsGWR_list = land_classes.map(func_lnr)




print('Recharge per land class (mm):', zonalStatsGWR_list)


# Annual precipitation
annualPrecip = annualWB.select('precipitation').mean().clip(geometry)

##################/
# Mean components of WB          #
##################

annualET_mean = annualET.reduceRegion({
  'reducer': ee.Reducer.mean(),
  'scale': 30,
  'geometry': geometry,
  'maxPixels': 1e9})

annualPrecip_mean = annualPrecip.reduceRegion({
  'reducer': ee.Reducer.mean(),
  'scale': 30,
  'geometry': geometry,
  'maxPixels': 1e9})

annualRunoff_mean = annualRunoff.reduceRegion({
  'reducer': ee.Reducer.mean(),
  'scale': 30,
  'geometry': geometry,
  'maxPixels': 1e9})

annualRecharge_mean = annualRecharge.reduceRegion({
  'reducer': ee.Reducer.mean(),
  'scale': 30,
  'geometry': geometry,
  'maxPixels': 1e9})

print('Mean annual precipitation (mm):', annualPrecip_mean.get('precipitation'))
print('Mean annual runoff (mm):', annualRunoff_mean.get('Q'))
print('Mean annual recharge (mm):', annualRecharge_mean.get('recharge_final'))
print('Mean annual ET (mm):', annualET_mean.get('ET'))


#######################################
#          Export images, creating charts specifying scale and region.     #
#######################################

Export.image.toDrive({
  'image': annualET,
  'description': 'annualET',
  'scale': 100,
  'region': geometry
})

Export.image.toDrive({
  'image': annualRecharge,
  'description': 'annualRecharge',
  'scale': 100,
  'region': geometry
})


# ANNUAL RECHARGE
titleGWR = {
 'title': 'Annual recharge (mm)',
 'hAxis': '{title': 'Year'},
 'vAxis': '{title': 'Groundwater recharge (mm)'},
}

chartGWR = ui.Chart.image.seriesByRegion({
 'imageCollection': annualWB,
 'regions': geometry,
 'reducer': ee.Reducer.mean(),
 'band': 'recharge_final',
 'scale': 500,
 'seriesProperty': 'SITE'
}).setOptions(titleGWR) \
 .setChartType('ColumnChart')

print(chartGWR)

# MONTHLY RECHARGE CHART

meanMonthlyWB =  ee.ImageCollection.fromImages(

def func_gyj (m):
    w = monthlyWB.filter(ee.Filter.eq('month', m)).mean()
    return w.set('month', m) \
            .set('system:time_start',(ee.Date.fromYMD(1, m, 1)).format('MMMMMM'))

  months.map(func_gyj
).flatten()



).flatten()
)

titleMonthlyGWR = {
  'title': 'Long-term avg. monthly recharge (mm)',
  'hAxis': '{title': 'Month'},
  'vAxis': '{title': 'Groundwater recharge (mm)'},}

chartMonthlyGWR = ui.Chart.image.seriesByRegion({
  'imageCollection': meanMonthlyWB,
  'regions': geometry,
  'reducer': ee.Reducer.mean(),
  'band': 'recharge_final',
  'scale': 10,
  'seriesProperty': 'SITE'}) \
  .setOptions(titleMonthlyGWR) \
  .setChartType('ColumnChart')

print(chartMonthlyGWR)

# MONTHLY PRECIPITATION CHART

titleMonthlyP = {
  'title': 'Long-term avg. monthly precipitation (mm)',
  'hAxis': '{title': 'Month'},
  'vAxis': '{title': 'Precipitation (mm)'},}

chartMonthlyP = ui.Chart.image.seriesByRegion({
  'imageCollection': meanMonthlyWB,
  'regions': geometry,
  'reducer': ee.Reducer.mean(),
  'band': 'precipitation',
  'scale': 2500,
  'seriesProperty': 'SITE'}) \
  .setOptions(titleMonthlyP) \
  .setChartType('ColumnChart')

print(chartMonthlyP)

#*****************************************************************
# Annual Precipitation
titleAnnualP = {
  'title': 'Annual precipitation (mm)',
  'hAxis': '{title': 'year'},
  'vAxis': '{title': 'Precipitation (mm)'},}

chartAnnualP = ui.Chart.image.seriesByRegion({
  'imageCollection': annualWB,
  'regions': geometry,
  'reducer': ee.Reducer.mean(),
  'band': 'precipitation',
  'scale': 2500,
  'seriesProperty': 'SITE'}) \
  .setOptions(titleAnnualP) \
  .setChartType('ColumnChart')

print(chartAnnualP)

# Annual Evapotranspiration
titleET = {
 'title': 'Annual evapotranspiration (mm)',
 'hAxis': '{title': 'Year'},
 'vAxis': '{title': 'ET (mm)'},
}

chartET = ui.Chart.image.seriesByRegion({
 'imageCollection': annualWB,
 'regions': geometry,
 'reducer': ee.Reducer.mean(),
 'band': 'ET',
 'scale': 500,
 'seriesProperty': 'SITE'
}).setOptions(titleET) \
 .setChartType('ColumnChart')

print(chartET)


#****************************************************************


# MONTHLY EVAPOTRANSPIRATION CHART
titleMonthlyET = {
  'title': 'Long-term avg. monthly evapotranspiration (mm)',
  'hAxis': '{title': 'Month'},
  'vAxis': '{title': 'ET (mm)'},}

chartMonthlyET = ui.Chart.image.seriesByRegion({
  'imageCollection': meanMonthlyWB,
  'regions': geometry,
  'reducer': ee.Reducer.mean(),
  'band': 'ET',
  'scale': 2500,
  'seriesProperty': 'SITE'}) \
  .setOptions(titleMonthlyET) \
  .setChartType('ColumnChart')

print(chartMonthlyET)

# MONTHLY RUNOFF CHART
titleMonthlyQ = {
  'title': 'Long-term avg. monthly runoff (mm)',
  'hAxis': '{title': 'Month'},
  'vAxis': '{title': 'Runoff (mm)'},}

chartMonthlyQ = ui.Chart.image.seriesByRegion({
  'imageCollection': meanMonthlyWB,
  'regions': geometry,
  'reducer': ee.Reducer.mean(),
  'band': 'Q',
  'scale': 2500,
  'seriesProperty': 'SITE'}) \
  .setOptions(titleMonthlyQ) \
  .setChartType('ColumnChart')

print(chartMonthlyQ)

# Find min and max  recharge for visualisation

rechargeMax = annualRecharge.reduceRegion({
  'reducer': ee.Reducer.max(),
  'geometry': geometry,
  'scale': 10, 'maxPixels': 1e9}).getNumber('recharge_final')

rechargeMin = annualRecharge.reduceRegion({
  'reducer': ee.Reducer.min(),
  'scale': 10,
  'geometry': geometry,
  'maxPixels': 1e9}).getNumber('recharge_final')

rechargeVis = {
  'min': -614,
  'max': 782,
  'palette': ['#FF8C00', '#FFFF00', '#32CD32', '#228B22', '#4169E1', '#000080']}

Map.addLayer(annualRecharge, rechargeVis,'Recharge (mm)')

#Map.addLayer(collectionPts, 'Collection points')

# LEGEND FOR ANNUAL RECHARGE

legendGWR = ui.Panel({
  'style': {
    'position': 'bottom-left',
    'padding': '8px 15px'
  }
})

# Create legend title
legendTitleGWR = ui.Label({
  'value': 'Recharge (mm)',
  'style': {
    'fontWeight': 'bold',
    'fontSize': '18px',
    'margin': '0 0 4px 0',
    'padding': '0'
    }
})

legendGWR.add(legendTitleGWR)

# Create the legend image
lonGWR = ee.Image.pixelLonLat().select('latitude')
gradientGWR = lonGWR.multiply((rechargeVis.max-rechargeVis.min)/100.0).add(rechargeVis.min)
legendImageGWR = gradientGWR.visualize(rechargeVis)

# create text on top of legend
panelGWR = ui.Panel({
'widgets': [
ui.Label(rechargeVis['max'])
],
})

legendGWR.add(panelGWR)

thumbnailGWR = ui.Thumbnail({
'image': legendImageGWR,
'params': '{bbox':'0, 0, 10, 100', 'dimensions':'8x150'},
'style': '{padding': '1px', 'position': 'bottom-center'}
})

# Add the thumbnail to the legend
legendGWR.add(thumbnailGWR)

#Create text on top of legend
panelGWR = ui.Panel({
'widgets': [
ui.Label(rechargeVis['min'])
],
})

legendGWR.add(panelGWR)
print(legendGWR)

# Legend runoff
legendQ = ui.Panel({
  'style': {
    'position': 'bottom-left',
    'padding': '8px 15px'
  }
})

# Create legend title
legendTitleQ = ui.Label({
  'value': 'Runoff (mm)',
  'style': {
    'fontWeight': 'bold',
    'fontSize': '18px',
    'margin': '0 0 4px 0',
    'padding': '0'
    }
})

legendQ.add(legendTitleQ)

# Create the legend image
lonQ = ee.Image.pixelLonLat().select('latitude')
gradientQ = lonQ.multiply((runoffVis.max-runoffVis.min)/100.0).add(runoffVis.min)
legendImageQ = gradientQ.visualize(runoffVis)

# create text on top of legend
panelQ = ui.Panel({
'widgets': [
ui.Label(runoffVis['max'])
],
})

legendQ.add(panelQ)

thumbnailQ = ui.Thumbnail({
'image': legendImageQ,
'params': '{bbox':'0, 0, 10, 100', 'dimensions':'8x150'},
'style': '{padding': '1px', 'position': 'bottom-center'}
})

# Add the thumbnail to the legend
legendQ.add(thumbnailQ)

#Create text on top of legend
panelQ = ui.Panel({
'widgets': [
ui.Label(runoffVis['min'])
],
})

legendQ.add(panelQ)
print(legendQ)

print('finished')

# MONTHLY RUNOFF


chartMonthlyQ = ui.Chart.image.seriesByRegion(meanMonthlyWB.select('precipitation'),
        meanMonthlyWB.select('S'), ee.Reducer.mean(), 2500, 'system:time_start') \
        .setOptions(titleMonthlyQ) \
        .setChartType('ColumnChart')

###########################################/
#                                      END                                          #
###########################################/
Map