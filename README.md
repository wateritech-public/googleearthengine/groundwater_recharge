# GEE-GroundwaterRecharge

## About the code:
This code has been written by **WaterITech** on basis of the MSc thesis "Sustainability
of groundwater resources in Kumasi, Ghana" by Estela Fernandes Potter and should be acknowledged if the script is ulilized.  

**WaterITech** has made translation from the original JS based code to Python and improved where necessary.
The updated code is made in colaboration with UENR [University of Energy and Natural Resources](https://uenr.edu.gh/) to allow assessment of groundwater recharge in Sunyani, Ghana.

It is a **beta version** distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY or FITNESS FOR A PARTICULAR PURPOSE.

## Requirements:

* **Land use classification:**
    The script needs inputs on land use classification - which could be derived via  a supervised classification utilizing Random Forest from Landsat 5,7, 8 and Sentinel-2. Please note that the land use classes are pre defined to match curve numbers.


* **Hydrological soil groups:**
    The script needs a hydrological soil group map.


* **shapefile of the area of interest:**
    The script needs a shapefile that defines the outline of the area of interest, which is utilized to calculate means of the water balance components.

NB: For rasters use the sample pyramiding policy.

NB: ALl assets needs to be uploaded to Google Earth Engine to enable processing.

NB: Further inspiration for geoprocessing via GEE in relation to water resources management [link](https://courses.spatialthoughts.com/gee-water-resources-management.html)


## Results:
with the scripts it is possible to extract precipitation and evaporation datasets based on remote data.
with pre defined curve numbers for different land use classes and hydrological soil groups runoff is calculated and the overall water balance components can then be derived:

### Precipitation:

<img src="Pics/AnnualPrecipitation.png"  width="80%" height="80%">

---

### Groundwater recharge:

<img src="Pics/AnnualRecharge.png"  width="80%" height="80%">

---

<img src="Pics/RechargeMap.png"  width="80%" height="80%">

