*****************************************************************************************
*SUSTINABILITY OF GROUNDWATER RESOURCES IN KUMASI, GHANA.				*
*MSc Thesis in Water Management, Faculty of Civil Engineering and Geosciences, TU DELFT.*
*Thesis can be found in repository: http://repository.tudelft.nl/			*
*****************************************************************************************
Author: Estela Fernandes Potter
Date: 8 October 2021

This file contains the necessary data for the Google Earth Engine scripts used to carry 
out groundwater recharge estimates and create land use maps.To access Google Earth Engine 
(GEE), you will need to log in using a registered Google account. The links below will 
direct you to the scripts. 

GROUNDWATER_RECHARGE_KUMASI: 	
https://code.earthengine.google.com/b4ebac49f28c93a633eb7bd4806f409b

LULC_KUMASI:
https://code.earthengine.google.com/ad1a0a88240e75e3dd727a6a350edb18

The following files are required to be uploaded as assets into GEE. This can be done for 
navigating to the *assets* tab located to the left of the Code Editor. 

!!! Note: when uploading ".tif" files, select *sample* as pyramiding policy. !!!

1. 2020 classification map over Kumasi: "classification2020_S2.tif"
2. 2013 classification map over Kumasi: "classification2013_L8.tif"
3. 2003 classification map over Kumasi: "classification2003.tif" (optional)
4. 1986 classification map over Kumasi: "classification1986_L5.tif"
5. Hydrological soil groups: "HSG.kumasi.tif"
6. Kumasi outline: "Kumasi_outline.zip" 


# Added by AN:
links to data is here:
https://data.4tu.nl/articles/dataset/Google_Earth_Engine_files_for_groundwater_recharge_in_Kumasi/16729888/1

link to main data page:
https://repository.tudelft.nl/islandora/object/uuid%3A25159639-accd-442b-8e84-b16abe32b571?collection=education